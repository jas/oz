Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: oz
Upstream-Contact: Chris Lalancette <clalance@redhat.com>
Source: https://github.com/clalancette/oz/wiki

Files: *
Copyright: Copyright (C) 2010,2011  Chris Lalancette <clalance@redhat.com>
           Copyright (C) 2012,2013  Chris Lalancette <clalancette@gmail.com>
License: LGPL-2.1
 Quoting oz-install:
 .
 # This library is free software; you can redistribute it and/or
 # modify it under the terms of the GNU Lesser General Public
 # License as published by the Free Software Foundation;
 # version 2.1 of the License.
 .
 # This library is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 # Lesser General Public License for more details.
 .
 # You should have received a copy of the GNU Lesser General Public
 # License along with this library; if not, write to the Free Software
 # Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 # On Debian systems, the complete text of the GNU Lesser General
 # Public License version 2.1 can be found in
 # /usr/share/common-licenses/LGPL-2.1
 .
 # Some files have different copyright headers, including:
 #
 # oz/FreeBSD.py
 # Copyright (C) 2013  harmw <harm@weites.com>
 # Copyright (C) 2013  Chris Lalancette <clalancette@gmail.com>
 # oz/RHEL_7.py
 # Copyright (C) 2013,2014  Chris Lalancette <clalancette@gmail.com>
 # Copyright (C) 2013  Ian McLeod <imcleod@redhat.com>

Files: debian/*
Copyright: Copyright (C) 2011 Richard Jones <rjones@redhat.com>
           Copyright (C) 2012-2015 Chris Lalancette <clalancette@gmail.com>
           Copyright (C) 2015-2022 Simon Josefsson <simon@josefsson.org>
License: LGPL-2.1+
 This was packaged for Debian by Richard Jones <rjones@redhat.com> on
 Thu, 22 Sep 2011 16:17:34 +0100, and later updated by Chris Lalancette in
 git during 2012-2015 and after 2015 updated by Simon Josefsson.
 .
 The Debian packaging is licensed under the LGPL version 2.1 or later.
